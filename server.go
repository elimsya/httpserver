package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type Server struct {
	addr        string
	httpServer  *http.Server
	closeSignal chan *CloseSignal
}

type CloseSignal struct {
}

//创建服务器
func NewServer(addr string) (server *Server) {
	shutDownServerSignal := make(chan *CloseSignal)
	server = &Server{
		addr: addr,
		httpServer: &http.Server{
			Addr: addr,
		},
		closeSignal: shutDownServerSignal,
	}
	go server.shutDownGracefully()
	return server
}

func (server Server) RegisterHandler(pattern string, handler http.HandlerFunc) {
	http.HandleFunc(pattern, handler)
}

func (server Server) ListenAndServe() error {
	err := server.httpServer.ListenAndServe()
	// 阻塞直到有关闭服务器的信号
	<-server.closeSignal
	return err
}

//关闭服务器
func (server Server) shutDownGracefully() {
	interruptSignal := make(chan os.Signal)
	signal.Notify(interruptSignal, syscall.SIGTERM, syscall.SIGINT, syscall.SIGQUIT)
	//阻塞直到有中断信号
	<-interruptSignal

	if err := server.shutDownHttpServer(5 * time.Second); nil != err {
		log.Fatalf("Server shutdown failed, err: %v\n", err)
	}
	log.Println("Server gracefully shutdown!")
	//退出进程
	close(server.closeSignal)
}

//关闭服务器
func (server Server) shutDownHttpServer(timeout time.Duration) error {
	//开始shutdown，并设置超时间
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer func() {
		//do something
		cancel()
	}()
	httpServer := server.httpServer
	return httpServer.Shutdown(ctx)
}
