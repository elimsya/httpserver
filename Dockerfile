#FROM golang:alpine as build
#MAINTAINER elims smile_yangy@163.com
#ENV GOPROXY=https://goproxy.cn,direct
#RUN apk add --no-cache git
#RUN go get gitee.com/elimsya/httpserver

#FROM scratch
#WORKDIR /home/bin
#COPY --from=build /go/bin/httpserver /home/bin/httpserver
#EXPOSE 8080
#ENTRYPOINT ["/home/bin/httpserver"]

FROM golang:alpine as build
WORKDIR /home/httpserver
COPY . /home/httpserver
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/httpserver  .

FROM scratch
WORKDIR /home/bin
COPY --from=build /home/httpserver/bin/httpserver /home/bin/httpserver
EXPOSE 8080
ENTRYPOINT ["/home/bin/httpserver"]