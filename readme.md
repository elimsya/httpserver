## 容器化httpserver
### 1. 编写Dockerfile
```dockerfile
FROM golang:alpine as build
MAINTAINER elims smile_yangy@163.com
ENV GOPROXY=https://goproxy.cn,direct
RUN apk add --no-cache git
RUN go get gitee.com/elimsya/httpserver

FROM alpine
WORKDIR /home/bin
COPY --from=build /go/bin/httpserver /home/bin/httpserver
EXPOSE 8080
ENTRYPOINT ["/home/bin/httpserver"]
```

### 2. 构建容器
```shell
docker build -t elims/httpserver:latest .
```

### 3. 将构建好的容器推送到docker hub中
```shell
# 登录docker hub
root@iZuf648xlh7jh524mzpsnvZ:/home/httpserver# docker login -u ilidan -p xxx

# 添加tag
root@iZuf648xlh7jh524mzpsnvZ:/home/httpserver# docker tag elims/httpserver:latest ilidan/httpserver:2.0

# 推送
root@iZuf648xlh7jh524mzpsnvZ:/home/httpserver# docker push ilidan/httpserver:2.0
The push refers to repository [docker.io/ilidan/httpserver]
a6633780fc40: Pushed 
e2eb06d8af82: Pushed 
2.0: digest: sha256:1f92f406c32f95718428e873829e0166551af4171e9cf9d2e266183683c5da19 size: 739
```

### 4. 启动容器
```shell
root@iZuf648xlh7jh524mzpsnvZ:/home/httpserver# docker pull ilidan/httpserver:2.0
2.0: Pulling from ilidan/httpserver
a0d0a0d46f8b: Already exists 
02892fca8a23: Pull complete 
Digest: sha256:1f92f406c32f95718428e873829e0166551af4171e9cf9d2e266183683c5da19
Status: Downloaded newer image for ilidan/httpserver:2.0
docker.io/ilidan/httpserver:2.0

root@iZuf648xlh7jh524mzpsnvZ:/home/httpserver# docker run -d -p 8080:8080 --name httpserver ilidan/httpserver:2.0 
6abb54064e3c336da2ebef432c3b29ea656ab36837a1ae80236a00532be9c2b4

root@iZuf648xlh7jh524mzpsnvZ:/home/httpserver# docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED         STATUS         PORTS                                       NAMES
6abb54064e3c   ilidan/httpserver:2.0   "/home/bin/httpserver"   5 seconds ago   Up 4 seconds   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp   httpserver
```
### 5. 进入容器并查看ip信息
```shell
# 查看进程id
root@iZuf648xlh7jh524mzpsnvZ:/home/httpserver# docker inspect 6abb54064e3c |grep id
            "Pid": 112945,
            "PidMode": "",
            "PidsLimit": null,
            "IOMaximumBandwidth": 0,
            "Image": "ilidan/httpserver:2.0",
            "Bridge": "",
                "bridge": {

# 查看容器ip信息
root@iZuf648xlh7jh524mzpsnvZ:/home/httpserver# nsenter -t 112945 -n ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
205: eth0@if206: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```